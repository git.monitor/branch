const config = require('../config.js');

var _ = require('lodash');
var restify = require('restify');
var NodeGit = require('nodegit');
var path = require('path');

var ready = NodeGit.Repository.open(path.resolve(config.repo));
var server = restify.createServer();

//-------GIT CACHES---------------------
var cache = {}; //Ease lookup time
function writeCache(parent, child, result) {
    _.set(cache, [parent, child], {
        mergeDate : result,
        writeTime : 0 //TODO actually log when something was added to the cache
    });
}

/*
 * Return value from the cache
 * - if it doesn't exist, return null (signifying that a new request should be made)
 * - if the result is too old, return null (signifying that a new request should be made)
 */
function getCache(parent, child) {

    //TODO invalidate cache
    return _.get(cache, [parent, child], undefined);
}

//----------CONFIGURE GIT----------------

function branchStatus(parent, child) {
    return NodeGit.Merge.base(repo, parent, child)
        .then(function (oid) {
            return NodeGit.Commit.lookup(repo, oid);
        })
        .then(function (commit) {
            return commit;
        });
}

function branchId(branch) {

    return NodeGit.Branch.lookup(repo, 'origin/' + branch, NodeGit.Branch.BRANCH.REMOTE)
        .then(function (reference) {
            return reference.target();
        });
}

function fetch() {
    console.log('fetching remote: `origin`');

    return NodeGit.Remote.lookup(repo, 'origin')
        .then(function (remote) {
            return remote.fetch();
        });
}

//----------CONFIGURE SERVER-------------

function getBranchStatus(req, res, next) {
    var parent = req.params.parent;
    var child = req.params.child;

    //TODO actually use cache
    Promise.all([branchId(parent), branchId(child)])
        .then(function (ids) {
            branchStatus(ids[0], ids[1]).done(function(commit) {
                writeCache(req.params.parent, req.params.child, commit);

                res.send({
                    date : commit.date(),
                    author : commit.author(),
                    message : commit.message()
                });

                next();
            });
        }, function () {

            //TODO find out how to properly forward errors
            res.send('ERROR Will Robinson!', arguments);
            next();
        });
}


//----------START SERVER------------------

var repo;
ready.done(function (_repo) {
    repo = _repo;
    server.get('/branch/:parent/:child', getBranchStatus);

    server.listen(8080, function () {
        console.log('%s listening at %s', server.name, server.url);
    });

    //Periodically refresh branches
    setInterval(fetch, config.fetchInterval);
});
